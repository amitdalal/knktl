FROM gcr.io/kaniko-project/executor:debug as kaniko_builder

FROM alpine

RUN apk --no-cache add coreutils ca-certificates bash curl git &&\
curl -sSL -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub &&\
curl -sSL -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk &&\
apk add glibc-2.28-r0.apk &&\
rm glibc-2.28-r0.apk &&\
rm -rf /var/cache/apk/*

COPY --from=kaniko_builder /kaniko /kaniko

ENV SSL_CERT_DIR=/kaniko/ssl/certs
ENV PATH="/kaniko:${PATH}"
ENV DOCKER_CONFIG /kaniko/.docker/
ENV HOME /root
ENV USER /root

RUN curl -sLO \
    "https://storage.googleapis.com/kubernetes-release/release/$(curl -s \
        https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"

RUN chmod +x ./kubectl && mv ./kubectl /bin/kubectl && kubectl version --client

COPY --from=registry.gitlab.com/gitlab-org/gitlabktl /usr/bin/gitlabktl /bin/
